- **0.4.0** (2022-03-02):
    - retry conflicting user updates
    - do not loop forever when downloading missing data
    - download likes relationships only for first-order tweets (matching a search_term)
    - fixed formatting in fancy output
    - refactored BaseDownloadersThread

- **0.3.7** (2022-02-03):
    - handle invalid next_token gracefully

- **0.3.6** (2021-07-01):
    - save likes (also) as relationships between tweets and users
    - improved statistics
    - improved performance (and efficiency, re rate-limit, DB-i/o, etc.)
    - fixed some minor bugs as always

- **0.3.5** (2021-06-28):
    - save `public_metrics` (likes, retweets, etc.)
    - better indexing
    - per-endpoint calculation of rate limits
    - update incomplete records of users or tweets
    - all kind of refactoring, linting, cleaning and bug-fixing

- **0.3.4** (2021-05-21):
    - handle connection errors more gracefully
    - allow multiple references to same tweet (retweet/reply/etc)
    - squat some bugs

- **0.3.3** (2021-04-28):
    - enable non-default location config files
    - minor bugfixes:
        - try to wait out 503 errors
        - float/integer TypeError in handling TemporaryApiResonseError
        - handle missing config values more gracefully

- **0.3.2** (2021-04-12):
    - some minor bug fixes

- **0.3.1** (2021-04-09):
    - Fix `ModuleNotFoundError: No module named 'twitterhistory.database'

- **0.3.0** (2021-04-09):
    - faster database operations (also re issue #5)
    - fixed NUL characters in some API responses (issue #9)
    - better error handling

- **0.2.4** (2021-02-10):
    - Fix Place.geom (issue #8)

- **0.2.3** (2021-02-02):
    - correct embarrassing typo
    - handle 1:n children of Place, User better

- **0.2.2** (2021-02-02):
    - allow database schema migration
    - change Country() primary key
    - fix previously ommited `user_mentions`
    - enable PostGIS if necessary

- **0.2.1** (2021-01-27):
    - updated README
    - minor fixes

- **0.2.0** (2021-01-27):
    - updated README
    - fixed typo in tweetdownloaderthread.py
    - published to PyPi
    - published to Zenodo

- **0.1.2** (2021-01-26):
    - use the academic research api endpoint /2/tweets/search/all

- **0.1.1** (2021-01-21):
    - bug fixes

- **0.1.0** (2021-01-21):
    - First working release
